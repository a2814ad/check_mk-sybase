## check_mk Sybase plugin

Plugin to monitor Sybase databases

## Installation

Add the sybase*.mkp to your check_mk server via extensions.

Add the client plugin to your check_mk client and edit the config

## Changes

1.6: Added Percent values for user levels, added agent bakery (untested)

## License

GPLv2
