#!/usr/bin/python
# -*- encoding: utf-8; py-indent-offset: 4 -*-

group = "checkparams"

subgroup_applications = _("Applications, Processes & Services")

try:
    register_check_parameters(
	 subgroup_applications,
	"sybase_databases",
	_("Sybase ASE Database usage levels"),
	Dictionary(
	    elements = [
		(   "levels",
			Tuple(
			    title = _("Usage Levels"),
			    elements = [
				Float( title = _("Warning if datasegment usage is above"), default_value = 80.0 ),
				Float( title = _("Critical if datasegment usage is above"), default_value = 90.0 ),
				Float( title = _("Warning if logsegemnt usage is above"), default_value = 50.0 ),
				Float( title = _("Critical if logsegemnt usage is above"), default_value = 75.0 ),
			    ],
			),
		),
	    ],
	    # optional_keys = ["levels", "expectedStrings"]
	),
	TextAscii(
	    title = _("Dataserver and database name"),
	    help = _("Name of the database server and name of the database separated by a space.")
	),
	"dict",
    )
except NameError:
    pass


try:
    register_check_parameters(
         subgroup_applications,
        "sybase_users",
        _("Sybase ASE Database users and running processes levels"),
        Dictionary(
            elements = [
                (   "userlevels",
                    Alternative(
                        title = _("User Levels"),
                        help = _("Logged in users. If percent values are used, 100% equals to the config value 'number of user connections' in sybase"),
                        elements = [
                            Tuple(
                                title = _("Users absolute values"),
                                elements = [
                                    Integer( title = _("Warning if logged in users are above"), default_value = 100 ),
                                    Integer( title = _("Critical if logged in users are above"), default_value = 200 ),
                                ],
                            ),
                            Tuple(
                                title = _("User Levels in percent of maximum"),
                                elements = [
                                    Float( title = _("Warning if logged in users are above %"), unit = "%", default_value = 50.0 ),
                                    Float( title = _("Critical if logged in users are above %"), unit = "%", default_value = 75.0 ),
                                ],
                            ),
                        ],
                    ),
                ),
                (   "runninglevels",
                    Alternative(
                        title = _("Running Processes Levels"),
                        help = _("Running processes or transactions. If percent values are used, 100% equals to 'max online engines' config value"),
                        elements = [
                            Tuple(
                                title = _("Running Levels absolute values"),
                                elements = [
                                    Integer( title = _("Warning if running processes are above"), default_value = 20 ),
                                    Integer( title = _("Critical if running processes are above"), default_value = 50 ),
                                ],
                            ),
                            Tuple(
                                title = _("Running Levels in percent of maximum"),
                                elements = [
                                    Float( title = _("Warning if running processes are above %"), unit = "%", default_value = 50.0 ),
                                    Float( title = _("Critical if running processes are above %"), unit = "%", default_value = 100.0 ),
                                ],
                            ),
                        ],
                    ),
                ),
            ],
            # optional_keys = ["levels", "expectedStrings"]
        ),
        TextAscii(
            title = _("Dataserver name"),
            help = _("Name of the database server")
        ),
        "dict",
    )
except NameError:
    pass

try:
    register_check_parameters(
         subgroup_applications,
        "sybase_locks",
        _("Sybase ASE Database Locks"),
        Dictionary(
            elements = [
                (   "levels_all",
                        Tuple(
                            title = _("All Locks Levels"),
                            elements = [
                                Integer( title = _("Warning if locks are above"), default_value = 10 ),
                                Integer( title = _("Critical if locks are above"), default_value = 50 ),
                            ],
                        ),
                ),
                (   "levels_shared",
                        Tuple(
                            title = _("Shared Locks Levels"),
                            elements = [
                                Integer( title = _("Warning if shared locks are above"), default_value = 10 ),
                                Integer( title = _("Critical if shared locks are above"), default_value = 50 ),
                            ],
                        ),
                ),
                (   "levels_exclusive",
                        Tuple(
                            title = _("Exclusive Locks Levels"),
                            elements = [
                                Integer( title = _("Warning if exclusive locks are above"), default_value = 1 ),
                                Integer( title = _("Critical if exclusive locks are above"), default_value = 5 ),
                            ],
                        ),
                ),
                (   "levels_update",
                        Tuple(
                            title = _("Update Locks Levels"),
                            elements = [
                                Integer( title = _("Warning if update locks are above"), default_value = 1 ),
                                Integer( title = _("Critical if update locks are above"), default_value = 5 ),
                            ],
                        ),
                ),
                (   "levels_demand",
                        Tuple(
                            title = _("Demand Locks Levels"),
                            elements = [
                                Integer( title = _("Warning if demand locks are above"), default_value = 1 ),
                                Integer( title = _("Critical if demand locks are above"), default_value = 5 ),
                            ],
                        ),
                ),
                (   "levels_blocking",
                        Tuple(
                            title = _("Blocking Locks Levels"),
                            elements = [
                                Integer( title = _("Warning if blocking locks are above"), default_value = 1 ),
                                Integer( title = _("Critical if blocking locks are above"), default_value = 5 ),
                            ],
                        ),
                ),

            ],
        ),
        TextAscii(
            title = _("Dataserver name"),
            help = _("Name of the database server")
        ),
        "dict",
    )
except NameError:
    pass


try:
    register_check_parameters(
         subgroup_applications,
        "sybase_cache",
        _("Sybase ASE Database Cache Hit Ratio"),
        Dictionary(
            elements = [
                (   "levels",
                        Tuple(
                            title = _("Cache Levels"),
                            elements = [
                                Float( title = _("Warning if cache hit ratio is below"), default_value = 95.0 ),
                                Float( title = _("Critical if cache hit ratio is below"), default_value = 90.0 ),
                            ],
                        ),
                ),
            ],
        ),
        TextAscii(
            title = _("Dataserver name"),
            help = _("Name of the database server")
        ),
        "dict",
    )
except NameError:
    pass

