#!/usr/bin/python

def perfometer_sybase_dbs(row, check_command, perf_data):
    datarname, dataused, dataunit, datawarn, datacrit, datamin, datamax = perf_data[0]
    logrname, logused, logunit, logwarn, logcrit, logmin, logmax = perf_data[1]
    dataprc=100.0 * float(dataused)/float(datamax)
    if float(logmax) > 0:
        logprc=100.0 * float(logused)/float(logmax)
    else:
        logprc=0.0
    datacolor="#66ff33"
    if(float(dataused) > float (datawarn)):
        datacolor="#ffff00"
    if(float(dataused) > float (datacrit)):
        datacolor="#ff3300"

    logcolor="#0066ff"
    if(float(logused) > float (logwarn)):
        logcolor="#9999ff"
    if(float(logused) > float (logcrit)):
        logcolor="#ff00ff"

    h = '<table><tr>'
    h += perfometer_td((100.0-dataprc)/2.0, "white")
    h += perfometer_td(dataprc/2.0, datacolor)
    h += perfometer_td(logprc/2.0, logcolor)
    h += perfometer_td((100.0-logprc)/2.0, "white")
    h += "</tr></table>"

    return "<span style=\"float: left; margin-left: 0.3em;\">%0.2f %%</span><span style=\"float: right; margin-right: 0.3em;\">%0.2f %%</span>" % (dataprc,logprc) , h

def perfometer_sybase_cache(row, check_command, perf_data):
    cachename, cacheval, cacheunit, cachewarn, cachecrit, cachemin, cachemax = perf_data[0]
    color="#66ff33"
    if cacheval < float(cachewarn):
         color="#ffff00"
    if cacheval < float(cachecrit):
        color="#ff3300"
    h = '<table><tr>'
    h += perfometer_td(cacheval,color)
    h += perfometer_td(100.0-cacheval, "white")
    h += "</tr></table>"
    return "%0.2f %%" % cacheval,h
    

perfometers["check_mk-sybase_databases"] = perfometer_sybase_dbs
perfometers["check_mk-sybase_cache"] = perfometer_sybase_cache
