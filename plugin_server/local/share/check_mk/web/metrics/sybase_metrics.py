# sybase metric infos:
metric_info["sybase_cache"] = {
        "title" : _("Cache"),
        "unit"  : "%",
        "color" : "34/a",
        }

metric_info["sybase_data"] = {
        "title" : _("Data"),
        "unit"  : "bytes",
        "color" : "34/a",
        }

metric_info["sybase_log"] = {
        "title" : _("Log"),
        "unit"  : "bytes",
        "color" : "44/a",
        }

metric_info["sybase_locks"] = {
        "title" : _("Locks"),
        "unit"  : "count",
        "color" : "46/a",
        }

metric_info["sybase_exclusive_locks"] = {
        "title" : _("Exclusive locks"),
        "unit"  : "count",
        "color" : "16/a",
        }

metric_info["sybase_update_locks"] = {
        "title" : _("Update locks"),
        "unit"  : "count",
        "color" : "46/a",
        }

metric_info["sybase_demand_locks"] = {
        "title" : _("Demand locks"),
        "unit"  : "count",
        "color" : "13/a",
        }

metric_info["sybase_blocking_locks"] = {
        "title" : _("Blocking locks"),
        "unit"  : "count",
        "color" : "16/a",
        }

metric_info["sybase_users"] = {
        "title" : _("Logged in users"),
        "unit"  : "count",
        "color" : "46/a",
        }

metric_info["sybase_running"] = {
        "title" : _("Running users"),
        "unit"  : "count",
        "color" : "34/a",
        }

# sybase perfometers:
perfometer_info.append({
    "type"      : "linear",
    "segments"  : [ "sybase_cache" ],
    "total"     : 100.0,
    })

perfometer_info.append(("dual", [
    {
    "type"      : "linear",
    "segments"  : [ "sybase_data(%)"],
    "total"     : 100.0,
    },
    {
    "type"      : "linear",
    "segments"  : [ "sybase_log(%)" ],
    "total"     : 100.0,
    }
    ]))

# sybase graphs

graph_info.append({
    "title"    : _("Sybase ASE Cache"),
    "metrics"  : [ 
        ( "sybase_cache", "area" ),
        ],
    "scalars"  : [
        "sybase_cache:warn",
        "sybase_cache:crit",
        ],
    "range"    : (0, 100),
    })

graph_info.append({
    "title"    : _("Sybase ASE Data"),
    "metrics"  : [ 
        ( "sybase_data", "area" ),
        ],
    "scalars"  : [
        "sybase_data:warn",
        "sybase_data:crit",
        ],
    })

graph_info.append({
    "title"    : _("Sybase ASE Log"),
    "metrics"  : [ 
        ( "sybase_log", "area" ),
        ],
    "scalars"  : [
        "sybase_log:warn",
        "sybase_log:crit",
        ],
    })

graph_info.append({
    "title"    : _("Sybase ASE Locks"),
    "metrics"  : [ 
        ( "sybase_locks", "area" ),
        ],
    "scalars"  : [
        "sybase_locks:warn",
        "sybase_locks:crit",
        ],
    })

graph_info.append({
    "title"    : _("Sybase ASE Shared locks"),
    "metrics"  : [ 
        ( "sybase_shared_locks", "area" ),
        ],
    "scalars"  : [
        "sybase_shared_locks:warn",
        "sybase_shared_locks:crit",
        ],
    })

graph_info.append({
    "title"    : _("Sybase ASE Exclusive locks"),
    "metrics"  : [ 
        ( "sybase_exclusive_locks", "area" ),
        ],
    "scalars"  : [
        "sybase_exclusive_locks:warn",
        "sybase_exclusive_locks:crit",
        ],
    })

graph_info.append({
    "title"    : _("Sybase ASE Update locks"),
    "metrics"  : [ 
        ( "sybase_update_locks", "area" ),
        ],
    "scalars"  : [
        "sybase_update_locks:warn",
        "sybase_update_locks:crit",
        ],
    })

graph_info.append({
    "title"    : _("Sybase ASE Demand locks"),
    "metrics"  : [ 
        ( "sybase_demand_locks", "area" ),
        ],
    "scalars"  : [
        "sybase_demand_locks:warn",
        "sybase_demand_locks:crit",
        ],
    })

graph_info.append({
    "title"    : _("Sybase ASE Blocking locks"),
    "metrics"  : [ 
        ( "sybase_blocking_locks", "area" ),
        ],
    "scalars"  : [
        "sybase_blocking_locks:warn",
        "sybase_blocking_locks:crit",
        ],
    })

graph_info.append({
    "title"    : _("Sybase ASE Users"),
    "metrics"  : [ 
        ( "sybase_users", "area" ),
        ( "sybase_running", "area" ),
        ],
    "scalars"  : [
        ("sybase_users:warn#00FF00", "Users warning"),
        ("sybase_users:crit#FF0080", "Users critical"),
        ("sybase_running:warn", "Running users warning"),
        ("sybase_running:crit", "Running users critical"),
        ],
    })


