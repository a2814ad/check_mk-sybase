#!/usr/bin/python
# -*- encoding: utf-8; py-indent-offset: 4 -*-

# Sybase Database Usage

# columns in output of sybase_databases:
# 0: Servername
# 1: Databasename
# 2: total size of data segments in MB
# 3: used size of data segments in MB
# 4: total size of log segments in MB, may be NULL
# 5: used size of log segments in MB, may be NULL
# 6-9: status - flags

# default: data used warn, critical, log used warn, critical
factory_settings["sybase_database_default_values"] = {
    "levels" : (80.0, 90.0, 75.0, 85.0)
}

def inventory_sybase_databases(info):
    for line in info:
        yield line[0] + line[1], "sybase_database_default_values"

def check_sybase_databases(item, params, info):
    warndata, critdata, warnlog, critlog = params["levels"]
    for line in info:
        server=line[0]
        database=line[1]
        if item == server + database:
            # check status flags
            status=int(line[6])
            status2=int(line[7])
            status3=int(line[8])
            status4=int(line[9])
            # see http://infocenter.sybase.com/help/index.jsp?topic=/com.sybase.infocenter.dc36274.1550/html/tables/X42615.htm
            databasestatus=""
            if status & 256:
                databasestatus="Database suspect, "
            if status2 & 16:
                databasestatus+="Database is offline, "
            # usage
            datasize=float(line[2])*1048576
            dataused=float(line[3])*1048576
            datausedpct=100*(dataused/datasize)
            try:
                logsize=float(line[4])*1048576
                logused=float(line[5])*1048576
                logusedpct=100*(logused/logsize)
            except:
                logsize=0.0
                logused=0.0
                logusedpct=0.0
            outstring="%sData used: %.2f%% Log used: %.2f%%" % (databasestatus, datausedpct, logusedpct)
            perfdata=[("sybase_data", dataused, datasize*warndata/100, datasize*critdata/100, 0.0, datasize),
                      ("sybase_log", logused, logsize*warnlog/100, logsize*critlog/100, 0.0, logsize)]
            if datausedpct > critdata or logusedpct > critlog or databasestatus:
                return 2, outstring, perfdata
            if datausedpct > warndata or logusedpct > warnlog:
                return 1, outstring, perfdata
            return 0, outstring, perfdata
    return 3, str(item) + "not found"
 
            
# check declaration
check_info["sybase_databases"] = {
    "service_description"     : "Sybase ASE Databases",
    "check_function"          : check_sybase_databases,
    "inventory_function"      : inventory_sybase_databases,
    'has_perfdata'            : True,
    "group"                   : "sybase_databases",
    "default_levels_variable" : "sybase_database_default_values",
}
