#!/usr/bin/python
# -*- encoding: utf-8; py-indent-offset: 4 -*-

# Sybase Cache


# default: number of cache warning/critical
factory_settings["sybase_cache_default_values"] = {
    "levels" : (95.0,90.0)
}

def inventory_sybase_cache(info):
    dataservers=[]
    for line in info:
        if line[0] not in dataservers:
            dataservers.append(line[0])
    for dataserver in dataservers:
        yield dataserver,"sybase_cache_default_values"

def check_sybase_cache(item, params, info):
    warncache, critcache = params["levels"]
    numcache=0.0
    for line in info:
        server=line[0]
        if item == server: # and line[1] == "default data cache":
            numcache = float(line[2])
    outstring="Cache: %.2f %%" % numcache
    perfdata=[("sybase_cache", numcache, warncache, critcache, 0.0, "%")]
    if numcache < critcache:
        return 2, outstring, perfdata
    if numcache < warncache:
        return 1, outstring, perfdata
    return 0, outstring, perfdata
 
            
# check declaration
check_info["sybase_cache"] = {
    "service_description"     : "Sybase ASE Cache",
    "check_function"          : check_sybase_cache,
    "inventory_function"      : inventory_sybase_cache,
    'has_perfdata':              True,
    "group"                   : "sybase_cache",
    "default_levels_variable" : "sybase_cache_default_values",
}
