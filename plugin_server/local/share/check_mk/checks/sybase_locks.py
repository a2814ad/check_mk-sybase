#!/usr/bin/python
# -*- encoding: utf-8; py-indent-offset: 4 -*-

# Sybase Locks


# default: number of locks warning/critical
factory_settings["sybase_locks_default_values"] = {
    "levels_all" : (10,50),
    "levels_exclusive" : (1,5),
    "levels_shared" : (10,50),
    "levels_update" : (1,5),
    "levels_blocking" : (1,5),
    "levels_demand" : (1,5),
}

def inventory_sybase_locks(info):
    dataservers=[]
    for line in info:
        if line[0] not in dataservers:
            dataservers.append(line[0])
    for dataserver in dataservers:
        yield dataserver,"sybase_locks_default_values"

# locks in sybase
# type value | status
# 0x1 | Exclusive table lock
# 0x2 | Shared table lock
# 0x3 | Exclusive intent lock
# 0x4 | Shared intent lock
# 0x5 | Exclusive page lock
# 0x6 | Shared page lock
# 0x7 | Update page lock
# 0x8 | Exclusive row lock
# 0x9 | Shared row lock
# 0xA | Update row lock
# 0xB | Shared next key lock
# 0x100 | Lock is blocking another process
# 0x200 | Demand lock

def check_sybase_locks(item, params, info):
    numlocks=0
    lockcount={
        "all": 0,
        "exclusive": 0,
        "shared": 0,
        "update": 0,
        "table_excl": 0,
        "table_sh": 0,
        "intent_excl": 0,
        "intent_sh": 0,
        "page_excl": 0,
        "page_sh": 0,
        "page_up": 0,
        "row_excl": 0,
        "row_sh": 0,
        "row_up": 0,
        "nk_sh": 0,
        "blocking": 0,
        "demand": 0
    }
    for line in info:
        server=line[0]
        if item == server:
            ltype = int(line[1])
            lcount = int(line[2])
            lockcount["all"] += lcount
            if ltype & 512 != 0:
                ltype -= 512
                lockcount["demand"] += lcount
            if ltype & 256 != 0:
                ltype -= 256
                lockcount["blocking"] += lcount
            if ltype == 1:
                lockcount["table_excl"] += lcount
                lockcount["exclusive"] += lcount
            if ltype == 2:
                lockcount["table_sh"] += lcount
                lockcount["shared"] += lcount
            if ltype == 3:
                lockcount["intent_excl"] += lcount
                lockcount["exclusive"] += lcount
            if ltype == 4:
                lockcount["intent_sh"] += lcount
                lockcount["shared"] += lcount
            if ltype == 5:
                lockcount["page_excl"] += lcount
                lockcount["exclusive"] += lcount
            if ltype == 6:
                lockcount["page_sh"] += lcount
                lockcount["shared"] += lcount
            if ltype == 7:
                lockcount["page_up"] += lcount
                lockcount["update"] += lcount
            if ltype == 8:
                lockcount["row_excl"] += lcount
                lockcount["exclusive"] += lcount
            if ltype == 9:
                lockcount["row_sh"] += lcount
                lockcount["shared"] += lcount
            if ltype == 10:
                lockcount["row_up"] += lcount
                lockcount["update"] += lcount
            if ltype == 11:
                lockcount["nk_sh"] += lcount
                lockcount["shared"] += lcount




    outstring="Locks: %d, Shared: %d, Exclusive: %d, Update: %d, Blocking: %d, Demand: %d" % (lockcount["all"],lockcount["shared"],lockcount["exclusive"],lockcount["update"],lockcount["blocking"],lockcount["demand"])
    perfdata=[("sybase_locks", lockcount["all"], params["levels_all"][0], params["levels_all"][1], 0, ""),
              ("sybase_shared_locks", lockcount["shared"], params["levels_shared"][0], params["levels_shared"][1], 0, ""),
              ("sybase_exclusive_locks", lockcount["exclusive"], params["levels_exclusive"][0], params["levels_exclusive"][1], 0, ""),
              ("sybase_update_locks", lockcount["update"], params["levels_update"][0], params["levels_update"][1], 0, ""),
              ("sybase_demand_locks", lockcount["demand"], params["levels_demand"][0], params["levels_demand"][1], 0, ""),
              ("sybase_blocking_locks", lockcount["blocking"], params["levels_blocking"][0], params["levels_blocking"][1], 0, ""),
             ]
    if lockcount["all"] > params["levels_all"][1] or lockcount["shared"] > params["levels_shared"][1] or lockcount["exclusive"] > params["levels_exclusive"][1] or lockcount["update"]> params["levels_update"][1] or lockcount["demand"] > params["levels_demand"][1] or lockcount["blocking"] > params["levels_blocking"][1] :
        return 2, outstring, perfdata
    if lockcount["all"] > params["levels_all"][0] or lockcount["shared"] > params["levels_shared"][0] or lockcount["exclusive"] > params["levels_exclusive"][0] or lockcount["update"]> params["levels_update"][0] or lockcount["demand"] > params["levels_demand"][0] or lockcount["blocking"] > params["levels_blocking"][0] :
        return 1, outstring, perfdata
    return 0, outstring, perfdata
 
            
# check declaration
check_info["sybase_locks"] = {
    "service_description"     : "Sybase ASE Locks",
    "check_function"          : check_sybase_locks,
    "inventory_function"      : inventory_sybase_locks,
    'has_perfdata':              True,
    "group"                   : "sybase_locks",
    "default_levels_variable" : "sybase_locks_default_values",
}
