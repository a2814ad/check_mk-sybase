#!/usr/bin/python
# -*- encoding: utf-8; py-indent-offset: 4 -*-

# Sybase Users

# columns in output:
# 0: Servername
# 1: count
# 2: status

# default: logged users warn, crit, running processes warn, crit
factory_settings["sybase_users_default_values"] = {
    "userlevels" : (50.0,75.0),
    "runninglevels" : (50.0, 100.0)
}

def inventory_sybase_users(info):
    dataservers=[]
    for line in info:
        if line[0] not in dataservers:
            dataservers.append(line[0])
    for dataserver in dataservers:
        yield dataserver,"sybase_users_default_values"

def check_sybase_users(item, params, info):
    p_warnusers, p_critusers = params["userlevels"]
    p_warnprocs, p_critprocs = params["runninglevels"]
    loggedusers=0
    runningprocs=0
    configuserconnections=5 # minimum default
    configengines=1 # minimum default
    for line in info:
        server=line[0]
        if item == server:
            if line[2].strip() == 'number of user connections':
                configuserconnections=int(line[1])
            elif line[2].strip() == 'max online engines':
                configengines=int(line[1])
            else:
                loggedusers+=int(line[1])
                if line[2].strip() == 'running' or line[2].strip() == 'runnable':
                    runningprocs+=int(line[1])
    
    if type(p_warnusers) == int:
        warnusers = p_warnusers
        critusers = p_critusers
    if type(p_warnprocs) == int:
        warnprocs = p_warnprocs
        critprocs = p_critprocs
    if type(p_warnusers) == float: # percent parameters
        warnusers = p_warnusers * configuserconnections / 100.0
        critusers = p_critusers * configuserconnections / 100.0
    if type(p_warnprocs) == float:
        warnprocs = p_warnprocs * configengines / 100.0
        critprocs = p_critprocs * configengines / 100.0
                
    outstring="Users: %d, running: %d" % (loggedusers, runningprocs)
    perfdata=[("sybase_users", loggedusers, warnusers, critusers, 0, configuserconnections),
              ("sybase_running", runningprocs, warnprocs, critprocs, 0, configuserconnections)]
    if loggedusers > critusers or runningprocs > critprocs:
        return 2, outstring, perfdata
    if loggedusers > warnusers or runningprocs > warnprocs:
        return 1, outstring, perfdata
    return 0, outstring, perfdata
 
            
# check declaration
check_info["sybase_users"] = {
    "service_description"     : "Sybase ASE Users",
    "check_function"          : check_sybase_users,
    "inventory_function"      : inventory_sybase_users,
    'has_perfdata':              True,
    "group"                   : "sybase_users",
    "default_levels_variable" : "sybase_users_default_values",
}
