title: Sybase ASE Users
agents: linux, aix, freebsd, macosx, netbsd, openbsd, solaris
catalog: databases
license: GPL
distribution: LRZ
description:
 This check monitors Sybase ASE database users. It measures logged users
 and running processes.

examples:
 # Set default levels of logged users and running processed.
 # Here warning at 100 logged users, critical at 200,
 # warning at 20 running processed, critical at 50
 sybase_users_default_values = (100,200,20,50)



 checks = [
    # Use levels like above
    # on all hosts with the tag "linux"
    (["linux"], ALL_HOSTS, "sybase_users", None, (100,200,20,50)),
 ]
perfdata:
  Two values: Logged in users and running processes

inventory:
  Searches all dataservers configured on the client

[parameters]
userswarning (int): Logged in users where a warning state is triggered
userscritical (int): Logged in users where a critical state is triggered
runningwarning (int): Running processes where a warning state is triggered
runningcritical (int): Running processes where a critical state is triggered

[configuration]
sybase_users_default_values (int, int, int, int): 4 ints: The default
 levels. These levels are used for inventorized checks. This variable
 is preset to {(100,200,20,50)}
